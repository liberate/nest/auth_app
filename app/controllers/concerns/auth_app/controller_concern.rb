module AuthApp
  # Include this in your application to enable authenticated users
  module ControllerConcern
    extend ActiveSupport::Concern

    included do
      protect_from_forgery with: :exception

      # filters
      before_action :set_cache_control_header
      before_action :check_authentication
      around_action :check_authorization

      # helpers
      helper AuthApp::Engine.helpers
      helper_method :current_user, :authenticated_user, :scoped_user
      helper_method :app_root_url
      helper_method :app_home_url

      # global instance attributes
      attr_accessor :authenticated_user
      attr_accessor :scoped_user
      attr_accessor :authorized_yet

      # inheritable class attributes
      class_attribute :authentication_required, :allowed_user_scopes
      class_attribute :authorization_required, :use_around_transaction
      class_attribute :permitted_actions

      # error rescues
      rescue_from ActionController::InvalidAuthenticityToken, with: :render_session_problem
      rescue_from AuthApp::UnauthenticatedRequest,   with: :render_unauthenticated
      rescue_from AuthApp::UnauthorizedRequest,      with: :render_unauthorized
      rescue_from AuthApp::ForbiddenRequest,         with: :render_unauthorized
      rescue_from AuthApp::SuspendedRequest,         with: :render_suspended
    end

    class_methods do
      def require_authentication(allowed_user_scope: nil)
        self.authentication_required = true
        if allowed_user_scope
          self.allowed_user_scopes ||= []
          self.allowed_user_scopes += [allowed_user_scope].flatten
        end
      end

      def optional_authentication(allowed_user_scope: nil)
        self.authentication_required = false
        if allowed_user_scope
          self.allowed_user_scopes ||= []
          self.allowed_user_scopes += [allowed_user_scope].flatten
        end
      end

      def require_authorization(rollback: false)
        self.authorization_required = true
        self.use_around_transaction = rollback
      end

      def optional_authorization
        self.authorization_required = false
        self.use_around_transaction = false
      end

      def permit(*actions)
        self.permitted_actions = actions.map &:to_s
      end
    end

    protected

    def current_user
      self.scoped_user || self.authenticated_user
      #self.authenticated_user || self.scoped_user
    end

    #
    # normal log in method, requiring a user's password
    #
    def log_in!(user, password, **kwargs)
      raise ArgumentError unless user.is_a?(User)
      if may_authenticate?(user)
        user.authenticate!(password, **kwargs)
        reset_session # important to prevent session fixation!
        self.authenticated_user = user
        session[session_key]   = user.id
        session['domain_id'] = user.domain_object.id if user.respond_to?(:domain_object)
      else
        raise AuthApp::UnauthorizedRequest
      end
    end

    #
    # set the session to this user, even without a password
    #
    def log_in_as(user)
      raise ArgumentError unless user.is_a?(User)
      if may_authenticate?(user)
        reset_session # important to prevent session fixation!
        self.authenticated_user = user
        session[session_key]   = user.id
        session['domain_id'] = user.domain_object.id if user.respond_to?(:domain_object)
      else
        raise AuthApp::UnauthorizedRequest
      end
    end

    #
    # temporarily set a scoped user session
    #
    # if there is already an authenticated user, we reset the session, except for admins
    # or when the authenticated user is the same as the scoped user.
    #
    def scoped_log_in(user, scope)
      if authenticated_user && authenticated_user != user && !authenticated_user.admin?
        self.authenticated_user = nil
        reset_session
      elsif authenticated_user && authenticated_user == user
        return # keep the authenticated user
      else
        session[scoped_session_key(scope)] = user.id
        self.scoped_user = user
      end
    end

    def reset_scoped_session
      session.keys.grep(/#{Regexp.escape(scoped_session_key(""))}/).each do |session_key|
        session[session_key] = nil
      end
    end

    # may override
    def alert_layout
      'application'
    end

    # may override
    def may_authenticate?(user)
      true
    end

    ##
    ## URLS
    ##

    def configured_url(name)
      if self.respond_to?("auth_app_#{name}")
        self.send("auth_app_#{name}")
      elsif name.is_a?(String) && name.include?('.')
        namespace, method = name.split('.')
        self.send(namespace).send(method)
      else
        self.main_app.send(name)
      end
    end

    def app_root_url
      configured_url(AuthApp.root_url)
    end

    def app_home_url
      configured_url(AuthApp.home_url)
    end

    ##
    ## PERMISSIONS
    ##

    def permit!(&block)
      if block_given?
        passed = yield
      else
        passed = true
      end
      unless passed
        raise AuthApp::ForbiddenRequest
      end
      self.authorized_yet = true
    end

    def permission_denied!
      raise AuthApp::ForbiddenRequest
    end

    private

    #
    # Finds the user in session['user_id'] or session['<scope>_scoped_user_id']
    #
    def load_session_user
      if session[session_key]
        self.authenticated_user = User.find(session[session_key])
      end
      if self.allowed_user_scopes&.any?
        self.allowed_user_scopes.each do |scope|
          if session[scoped_session_key(scope)]
            self.scoped_user = User.find(session[scoped_session_key(scope)])
          end
        end
      end
    rescue ActiveRecord::RecordNotFound
      raise AuthApp::Error, "Could not find the user id in the current session cookie"
    end

    def scoped_session_key(scope)
      "#{scope}_scoped_user_id"
    end

    def session_key
      "user_id"
    end

    # must be overridden
    #def is_authorized?(user)
    #  false
    #end

    # probably should not override
    #def may_authenticate?(user)
    #  true
    #end

    def check_authentication
      load_session_user
      if self.class.authentication_required?
        if !current_user
          raise AuthApp::UnauthenticatedRequest
        end
      end
    end

    def check_authorization
      if self.class.authorization_required?
        self.authorized_yet = false
        if self.class.use_around_transaction?
          ActiveRecord::Base.transaction do
            yield
          end
        else
          yield
        end
        if !authorized?
          # we have the dredded double render problem. the scenario is this: the
          # page has already been rendered, but we have just discovered that
          # permit!() was never called. what do do? rails does not officially
          # support throwing out the response and creating a new one.
          # this hacky code works, but no idea how long it will work.
          self.response_body = nil
          @_response_body = nil
          raise AuthApp::ForbiddenRequest
        end
      else
        yield
      end
    end

    def set_cache_control_header
      # don't use 'no-cache'. only 'no-store' will prevent the browser from caching pages.
      response.headers["Cache-Control"] = "max-age=0, private, no-store"
    end

    def authorized?
      self.authorized_yet || self.permitted_actions&.include?(params[:action])
    end

    ##
    ## RENDER ERRORS
    ##

    def render_session_problem
      if session[:cookies_are_enabled] != true
        flash.now[:danger] = t(:cookies_not_enabled)
      else
        flash.now[:danger] = t(:session_expired)
      end
      #reset_session
      render 'auth_app/sessions/new'
    end

    def render_unauthenticated
      if request.method == "GET"
        session[:redirect_path] = request.fullpath
      end
      redirect_to auth_app.new_session_url
    end

    def render_unauthorized
      render layout: alert_layout, template: "auth_app/errors/unauthorized", status: 403
    end

    def render_suspended
      render layout: alert_layout, template: "auth_app/errors/suspended"
    end

  end
end
