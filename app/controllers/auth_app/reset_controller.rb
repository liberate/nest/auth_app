#
# Abstract super class of reset_with_code_controller and reset_with_email_controller
#
module AuthApp
  class ResetController < ::ApplicationController
    before_action do
      flash.now[:danger] = []
      @errors = {}
    end

    optional_authentication
    optional_authorization

    def enter_password
      raise Error, "not yet implemented"
    end

    protected

    # Checks password based on params :new_password and :password_confirmation
    # @return [Boolean]
    def password_ok?
      if !@user.password_strong_enough?(params[:new_password])
        @show_password_tips = true
        @zxcvbn_suggestion = PasswordTester.zxcvbn_suggestion(params[:new_password])
        @errors[:new_password] = "Password is not strong enough"
        false
      elsif params[:new_password] != params[:password_confirmation]
        @errors[:password_confirmation] = t(:password_confirmation_error)
        false
      else
        true
      end
    end

    def param_recovery_login_valid?
      if params[:recovery_login].blank?
        @errors[:recovery_login] = "Recovery login is missing"
        false
      else
        true
      end
    end

    def param_recovery_email_valid?
      if params[:recovery_email].blank?
        @errors[:recovery_email] = t('errors.format', attribute: t(:recovery_email), message: t('errors.messages.blank'))
        false
      elsif ValidatesEmailFormatOf.validate_email_format(params[:recovery_email]) != nil
        @errors[:recovery_email] = t('errors.format', attribute: t(:recovery_email), message: t('errors.messages.invalid'))
        false
      else
        true
      end
    end

    # Finds user from sgid stored on param or session :recovery_user
    # @param service [String] "recovery_code" or "recovery_token"
    # @return [User,nil]
    def get_user_from_sgid(service)
      params[:recovery_user] ||= session[:recovery_user]

      if params[:recovery_user]
        GlobalID::Locator.locate_signed(params[:recovery_user], for: service).tap do |user|
          if user.nil?
            Rails.logger.warn "invalid recovery_user sgid %s" % params[:recovery_user]
          end
          return user
        end
      end
    end
  end
end
