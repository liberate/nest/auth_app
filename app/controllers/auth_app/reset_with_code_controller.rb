#
# reset passwords using a recovery code
#
# step 1. GET enter-code
# setp 2. PUT check-code
# step 3. GET enter-password
# step 4. PUT update-password
#
class AuthApp::ResetWithCodeController < AuthApp::ResetController
  # get /reset-with-code/enter-code
  def enter_code
  end

  # put /reset-with-code/check-code
  def check_code
    return render(:enter_code) unless username_and_code_present?

    @user = User.find_by_login(params[:recovery_login])

    if @user&.authenticated?(params[:recovery_code], service: :recovery_code)
      params[:recovery_secret] ||= params[:recovery_code]
      render :enter_password
    else
      flash.now[:danger] = I18n.t(:username_or_password_is_incorrect)
      @errors[:recovery_login] = true
      @errors[:recovery_code] = true
      render :enter_code
    end
  end

  # get /reset-with-code/enter-password
  def enter_password
    @user = get_user_from_sgid('recovery_code')
    if @user
      render :enter_password
    else
      redirect_to reset_with_code_url
    end
  end

  # put /reset-with-code/update-password
  #
  # This resets a user's password using a recovery code.
  #
  # To successfully login with this request you need the following params:
  #
  #   new_password | new password that passes zxcvbn
  #   password_confirmation |  eq to new password
  #   recovery_user | signed global id of user
  #   recovery_secret | recovery code
  #
  def update_password
    code = params[:recovery_secret]
    @user = get_user_from_sgid(:recovery_code)
    if !@user.is_a?(User)
      flash.now[:danger] = "Password reset token has expired"
    elsif code.empty?
      @errors[:recovery_secret] = "Recovery code is required"
    elsif !code_is_valid?(@user, code)
      @errors[:recovery_secret] = "Recovery code is invalid"
    elsif !password_ok?
      # sets @errors
    end

    if @errors.present? || flash.now[:danger].present?
      render :enter_password
    else
      @user.reset_password_with_recovery_code!(
        new_password:          params[:new_password],
        password_confirmation: params[:password_confirmation],
        recovery_code:        code
      )
      log_in! @user, params[:new_password]
      redirect_to app_home_url
    end
  rescue StandardError => exc
    flash.now[:danger] = exc.to_s
    render :enter_password
  end

  protected

  def username_and_code_present?
    if params[:recovery_login].blank?
      @errors[:recovery_login] = t(:username_is_required)
    end
    if params[:recovery_code].blank?
      @errors[:recovery_code] = t(:recovery_is_required)
    end
    return @errors.empty?
  end

  def code_is_valid?(user, code)
    return AuthApp::Service::RecoveryCode.new(user: user, password: code).authenticated?
  rescue
    return false
  end

end
