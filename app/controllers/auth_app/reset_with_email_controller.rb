#
# Reset password using alternate email and a token.
#
# step 1. GET enter_email
# step 2. PUT check email -- confirm email and username combination
# step 3. GET enter password
# step 4. PUT update password -- confirm user and token combination
#
class AuthApp::ResetWithEmailController < AuthApp::ResetController
  # get /reset-with-email/enter-email
  def enter_email
  end

  # put /reset-with-email/check-email
  def check_email
    @user = if AuthApp.login_is_email
              if param_recovery_email_valid?
                get_user_from_recovery_email
              end
            else
              if param_recovery_email_valid? && param_recovery_login_valid?
                get_user_from_username_and_email
              end
            end

    if @user.present?
      if send_token_email(user: @user, email: params[:recovery_email])
        # identifies the current user in recovery does not authenticated them
        session[:recovery_user] = @user.recovery_email_sgid
        redirect_to reset_with_email_enter_password_url
      end
    else
      flash.now[:danger] << t(:username_or_recovery_email_is_incorrect)
      render :enter_email
    end
  end

  # get /reset-with-email/enter-password
  # get /reset-my-password/:recovery_user/:token
  def enter_password
    @user = get_user_from_sgid('recovery_token')
    if @user
      if params[:token] && token_is_valid?(@user, params[:token])
        params[:recovery_secret] = params[:token]
      end
      render :enter_password
    else
      redirect_to reset_with_email_url
    end
  end

  # put /reset-with-email/update-password
  def update_password
    token = params[:recovery_secret]
    @user = get_user_from_sgid(:recovery_token)
    if !@user.is_a?(User)
      flash.now[:danger] = "Password reset token has expired"
    elsif token.empty?
      @errors[:recovery_secret] = "Password reset token is required"
    elsif token_has_expired?(@user)
      @errors[:recovery_secret] = "Password reset token has expired"
    elsif !token_is_valid?(@user, token)
      @errors[:recovery_secret] = "Password reset token is invalid"
    elsif !password_ok?
      # sets @errors
    end

    if @errors.present? || flash.now[:danger].present?
      render :enter_password
    else
      @user.reset_password_with_recovery_token!(
        new_password:          params[:new_password],
        password_confirmation: params[:password_confirmation],
        recovery_token:        token
      )
      log_in! @user, params[:new_password]
      redirect_to app_home_url
    end
  rescue StandardError => exc
    flash.now[:danger] = exc.to_s
    render :enter_password
  end

  private

  # Creates a recovery token and sends the user an email with a reset link
  # or sets @errors and returns false
  #
  # See AuthApp::ApplicationHelper and AuthApp::Mailer
  #
  # @param user [User] User to send recovery token email to
  # @param email [String] valid recovery email
  # @return [Boolean]
  def send_token_email(user:, email:)
    unless user.recovery_email_valid?(email)
      @errors[:recovery_email] = user.errors.full_message(:recovery_email, user.errors.generate_message(:recovery_email))
      return false
    end
    if helpers.send_password_reset_email(user, email)
      flash[:success] = t(:check_email_for_token)
      return true
    else
      @errors[:recovery_email] = "Could not create recovery token"
      return false
    end
  end

  # Finds user using param :recovery_email
  #
  # @return [User, Nil]
  def get_user_from_recovery_email
    user = User.find_by(email: params[:recovery_email])
    if user.present?
      return user
    else
      @errors[:recovery_email] = true
      return nil
    end
  end

  # Finds user using param :recovery_login and :recovery_email
  # @return [User, Nil]
  def get_user_from_username_and_email
    user = User.find_by_login(params[:recovery_login])
    if user&.recovery_email_valid?(params[:recovery_email])
      return user
    else
      @errors[:recovery_login] = true
      @errors[:recovery_email] = true
      return nil
    end
  end

  def token_is_valid?(user, token)
    if AuthApp::Service::RecoveryToken.new(user: user, password: token).authenticated?
      @token_valid = true # used in enter_password.html.haml
      return true
    end
  rescue
    return false
  end

  def token_has_expired?(user)
    user.recovery_token_expires_at < Time.now
  end

end
