module AuthApp
  class SessionsController < ::ApplicationController
    protect_from_forgery with: :exception
    optional_authentication
    optional_authorization

    def new
      render :new
    end

    def create
      user = find_user
      return handle_no_user if user.nil?
      log_in!(user, params[:password])
      redirect_to(session[:redirect_path] || app_home_url)
    rescue UnauthenticatedRequest
      handle_authenticate_failure(user)
    rescue SuspendedRequest
      handle_authenticate_unavailable(user)
    rescue UnauthorizedRequest, ForbiddenRequest
      handle_authenticate_forbidden(user)
    end

    def destroy
      if current_user
        if params[:return_to_admin]
          reset_scoped_session
          flash[:info] = "Temporary user session ended"
        else
          reset_session
          flash[:info] = "You have been logged out"
        end
      end
      redirect_to app_root_url
    end

    protected

    def handle_no_user
      log_auth_failure
      render_incorrect
    end

    def handle_authenticate_failure(user)
      log_auth_failure(user)
      render_incorrect
    end

    def handle_authenticate_unavailable(user)
      log_auth_failure(user)
      render_unavailable
    end

    def handle_authenticate_forbidden(user)
      log_auth_failure(user)
      render_forbidden
    end

    def log_auth_failure(user = nil)
      #params[:username]
      #request.remote_ip
    end

    private

    def find_user
      login = (params[:login]||'').downcase.strip
      User.find_by_login(login)
    end

    def render_incorrect
      flash.now[:danger] = I18n.t(:username_or_password_is_incorrect)
      render :new
    end

    def render_unavailable
      flash.now[:danger] = I18n.t(:user_maintenance)
      render :new
    end

    def render_forbidden
      flash.now[:danger] = I18n.t(:user_forbidden)
      render :new
    end
  end
end
