module AuthApp
  module ApplicationHelper
    def logout_button
      render 'auth_app/sessions/logout_button'
    end

    # Creates new token and sends reset email
    # @param user [User]
    # @param email [String] if missing it users user#email
    # @return [Boolean]
    def send_password_reset_email(user, email = nil, domain = nil )
      ActiveSupport::Deprecation.warn('calling send_password_reset_email with domain is deprecated') if domain.present?

      email ||= user.email

      service = AuthApp.services.fetch(:recovery_token).new(user: user)
      token = service.reset!

      return false unless token.present? && user.recovery_token.present?

      AuthApp::Mailer.reset_password(
        user: user,
        recipient: email,
        link: auth_app.reset_password_link_url(recovery_user: user.recovery_email_sgid, token: token)
      ).deliver_later
      return true
    end
  end
end
