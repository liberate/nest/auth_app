module AuthApp
  #
  # A recovery email allows a user to reset their password.
  #
  # The recovery_email field may contain 1 or 2 recovery addresses, separated
  # by spaces.
  #
  # These addresses are stored in password digest form. For older users, their
  # recovery_email might still be in cleartext, so we support that too.
  #
  # If AuthApp.allow_recovery_from_primary_email is set
  # then users may also reset their password using their default email
  #
  module UserRecoveryEmail
    extend ActiveSupport::Concern

    included do
      validate :validate_recovery_email
      before_save :hash_recovery_email
    end

    # returns true if the email argument matches one of our
    # saved recovery emails or self.email if configured
    #
    def recovery_email_valid?(email)
      email = email.strip.downcase

      if AuthApp.allow_recovery_from_primary_email && self.email == email
        return true
      end

      if recovery_email.present?
        recovery_email_array.each do |digest|
          if is_argon2?(digest)
            return true if RbNaCl::PasswordHash.argon2_valid?(email, digest)
          else
            Rails.logger.info "detected unhashed recovery email for user #{id}"
            return true if digest.strip.downcase == email
          end
        end
      end

      return false
    end

    # signed global id for the reset password pages
    def recovery_email_sgid
      expires_at = recovery_token_expires_at ||
        Time.now.advance(hours: AuthApp::Service::RecoveryToken.token_expiration.hours)
      to_sgid(for: 'recovery_token', expires_at: expires_at).to_s
    end

    protected

    #
    # only allow well formatted email addresses
    #
    def validate_recovery_email
      if recovery_email.present? && recovery_email_changed?
        recovery_email_array.each do |email|
          if ValidatesEmailFormatOf.validate_email_format(email) != nil
            errors.add :recovery_email, I18n.t('errors.messages.invalid')
            break
          end
        end
      end
    end

    # create a digest of the recovery emails, so that we don't store what
    # the actual email address is.
    #
    # because the recovery_email column is a string, we can only store at most
    # two digests in the available space.
    #
    def hash_recovery_email
      if recovery_email.present? && recovery_email_changed? && !is_argon2?(recovery_email)
        self.recovery_email = recovery_email_array
                                .map {|e| RbNaCl::PasswordHash.argon2_str(e.strip.downcase) }
                                .join(' ')
      end
    end

    private

    # allow a max of two alternate email, separated by whitespace or commas
    def recovery_email_array
      if is_argon2?(recovery_email)
        self.recovery_email.split(" ")[0..1]
      else
        self.recovery_email.split(/[\s,]+/)[0..1]
      end
    end

    def is_argon2?(str)
      /\A\$argon2i?d?\$/.match? str
    end
  end
end
