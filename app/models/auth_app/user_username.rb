module AuthApp
#
# A concern that handles the behavior of user handles.
#
# This includes restrictions on usernames, validating usernames,
# finding users by username.
#
#
  module UserUsername
    extend ActiveSupport::Concern

    # don't change this without also modifying the users.username column width:
    MAX_USERNAME_LENGTH = 40

    # require number or letter as first digit, don't allow two symbols in a row
    USERNAME_RE = /\A[a-z0-9]+([-_\.]?[a-z0-9])+\z/

    included do
      before_validation :generate_username

      validates :username,
        presence: true,
        format: {with: USERNAME_RE},
        length: {in: 2..MAX_USERNAME_LENGTH}

      validate :validate_username

      before_create :downcase_username
    end

    def username=(username)
      if new_record?
        write_attribute(:username, username)
      else
        raise ArgumentError, 'attempt to change username: use update_username!()'
      end
    end

    def login
      if AuthApp.login_is_email
        self.email
      elsif domain
        [username, domain].join('@')
      else
        username
      end
    end

    def address
      [username, domain].join('@')
    end

    #
    # A username change will alter the following db fields:
    #
    # * users.username
    # * users.email
    # * mailboxes.username
    # * mailboxes.address
    # * aliases.source
    #
    # They must all be changed atomically, or everything goes to shit.
    #
    def update_username!(new_username)
      if self.username == new_username || !new_username.present?
        return true
      else
        User.transaction do
          #if mailbox && mailbox.username != new_username
          #  self.pause # unpaused with EnableUserTask
          #  mailbox.update_username! new_username
          #end
          write_attribute(:username, new_username)
          save!
        end
      end
    end

    class_methods do
      #
      # A "login" can be any number of different values:
      #
      # (1) username@domain
      # (2) username
      # (3) email
      #
      # email is given lowest precedence and is often not
      # the same as username@domain.
      #
      def find_by_login(login)
        if !login.present?
          return nil
        elsif login.count('@') != 0
          if column_names.include?('domain')
            userpart, domainpart = login.split('@')
            find_by(username: userpart, domain: domainpart) || find_by(email: login)
          else
            find_by(email: login)
          end
        elsif AuthApp.login_is_email
          return nil
        elsif AuthApp.default_domain.nil?
          self.find_by(username: login)
        else
          self.find_by(username: login, domain: AuthApp.default_domain)
        end
      end

      #
      # Checks to see that the username is in a format
      # that we consider to be acceptable
      #
      def username_valid?(username)
        username_allowed?(username) &&
        username.length <= MAX_USERNAME_LENGTH &&
        username =~ USERNAME_RE
      end

      #
      # returns true if the username is not in the list of forbidden usernames, or
      # ASCII homographs of forbidden usernames.
      #
      def username_allowed?(username)
        if AuthApp.forbidden_usernames
          AuthApp.forbidden_regexps ||= begin
            Regexp.union(
              AuthApp.forbidden_usernames.map { |uname|
                Regexp.new(
                  "\\A" +
                    uname.
                    gsub(/[o0]/, '[o0]').
                    gsub(/[i1l]/, '[i1l]').
                    gsub(/e/, '[e3]').
                    gsub(/s/, '[s5]').
                    gsub(/b/, '[b8]').
                    gsub(/[_-]/, "([\\._-]|)").
                    gsub(/\*/, '.*') +
                  "[s0-9_-]*\\z"
                )
              }
            )
          end
          !AuthApp.forbidden_regexps.match(username)
        else
          true
        end
      end

      def username_taken?(username, domain)
        return !!User.find_by(username: username, domain: domain)
      end
    end

    private

    RANDOM_USERNAME_CHARSET = %w[a b c d e f g h i j k m n p q r s t u v w x y z 2 3 4 5 6 7 8 9]

    def generate_username
      if self.username.nil?
        if email
          uname = email.split('@').first
          uname = I18n.transliterate(uname).downcase
          uname = uname.gsub(/[^a-z0-9_-]/, '_').gsub(/_+/, '_')
        end
        while uname.nil? || User.username_taken?(uname, self.domain) || !User.username_valid?(uname)
          uname = SecureRandom.random_bytes(6).each_byte.map {|byte|
            RANDOM_USERNAME_CHARSET[byte % RANDOM_USERNAME_CHARSET.length]
          }.join
        end
        write_attribute(:username, uname)
      end
    end

    def validate_username
      if username_changed?
        # update_email if self.respond_to?(:email) TODO: if and only if email is linked to username
        if defined?(Domain) && self.domain.present? && Domain.find_by(domain: self.domain).nil?
          errors.add :base, I18n.t('domain_invalid')
        end
        if User.username_taken?(self.username, self.domain) && errors[:username].empty?
          errors.add 'username', I18n.t('errors.messages.taken')
        end
        if !User.username_allowed?(self.username) && errors[:username].empty?
          errors.add 'username', I18n.t('errors.messages.taken')
        end
      end
    end

    # before_create
    def downcase_username
      write_attribute(:username, self.username.downcase)
    end
  end
end