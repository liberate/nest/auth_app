module AuthApp
  # This module provides User authentication methods, and is intended
  # to be included in a Rails User class
  module UserAuthentication
    extend ActiveSupport::Concern

    included do
      before_create :hash_primary_secret

      # set this attribute when creating a new user to set the digest
      # of the primary_secret
      attribute :password

      #
      # To support legacy schema:
      #
      #alias_attribute :primary_secret,            :crypted_password
      #alias_attribute :recovery_secret,           :recovery_code
      #alias_attribute :recovery_token,            :password_token
      #alias_attribute :recovery_token_expires_at, :password_token_expires_at
    end

    class_methods do
      def new_digest(pw)
        AuthApp.new_digest(pw)
      end
    end

    # is this a valid password?, erroring if not
    def authenticate!(password, **kwargs)
      AuthApp::Service.authenticate!(user: self, password: password, **kwargs)
    end

    # is this a valid password?, returning false
    def authenticated?(password, **kwargs)
      AuthApp::Service.authenticated?(user: self, password: password, **kwargs)
    end

    # Update the password for the given service.
    # Does not authenticate first.
    #
    # @param password [String] new password
    # @param password_confirmation [String,Nil] password confirmation
    # @param service [Symbol,Nil] service to update
    # @return [self]
    def update_password!(password:, password_confirmation: nil, service: :primary)
      service = AuthApp.services.fetch(service).new(user: self, password: password, password_confirmation: password_confirmation)
      if service.valid?
        service.update!
        self
      else
        raise PasswordUpdateError.new(service.errors)
      end
    end

    # Resets password with Service::RecoveryCode
    #
    # @param new_password [String] new password
    # @param password_confirmation [String] password confirmation
    # @param recovery_code [String] recovery code
    def reset_password_with_recovery_code!(new_password:, password_confirmation:, recovery_code:)
      recovery_code = AuthApp.parse_random_code(recovery_code)

      if authenticated?(recovery_code, service: :recovery_code)
        update_password!(password: new_password, password_confirmation: password_confirmation)
      else
        errors.add(:base, t(:invalidate_recovery_code))
        raise ActiveRecord::RecordInvalid, self
      end
    end

    # Resets password with Service::RecoveryToken
    #
    # @param new_password [String] new password
    # @param password_confirmation [String] password confirmation
    # @param recovery_token [String] recovery token
    def reset_password_with_recovery_token!(new_password:, password_confirmation:, recovery_token:)
      if authenticated?(recovery_token, service: :recovery_token)
        User.transaction do
          update_password!(password: new_password, password_confirmation: password_confirmation)
          AuthApp::Service::RecoveryToken.new(user: self).clear!
        end
      else
        errors.add(:base, I18n.t(:invalid_recovery_token))
        raise ActiveRecord::RecordInvalid, self
      end
    end

    # Sets a new recovery code using the current password.
    #
    def update_recovery!(password:, new_recovery:)
      if authenticated?(password)
        update_password!(password: new_recovery, service: :recovery)
      else
        errors.add(:base, I18n.t(:password_is_incorrect))
        raise ActiveRecord::RecordInvalid, self
      end
    end

    def password_strong_enough?(pw)
      AuthApp::PasswordTester.test(pw, user_inputs: [self.username])
    end

    # Reset Service::RecoveryToken
    #
    # @param password [String] the user's primary password
    # @return [String] a randomly generated token
    def create_random_recovery!(password)
      authenticate!(password)
      AuthApp::Service::RecoveryToken.new(user: self).reset!
    end

    # Reset Service::RecoveryCode
    #
    # @param password [String] the user's primary password
    # @return [String] a randomly generated token
    def create_random_code!(password)
      authenticate!(password)
      AuthApp::Service::RecoveryCode.new(user: self).reset!
    end

    def update_main_password!(password:, new_password:)
      raise ActiveSupport::DeprecationException.new("update_main_password! is deprecated. use update_password!.")
    end

    def hard_reset_password!(new_password:, new_recovery_code: nil)
      raise ActiveSupport::DeprecationException.new("hard_reset_password! is deprecated. use update_password!")
    end

    def reset_password!(...)
      raise ActiveSupport::DeprecationException.new("reset_password! is deprecated. use update_password!.")
    end

    ##
    ## AUTHENTICATION
    ##

    #
    # This will update the last login timestamp and update their digest if necessary
    #
    def touch_user(password)
      service = AuthApp.services.fetch(:primary).new(user: self, password: password)
      service.authenticate! do |user|
        service.upgrade_digest if service.digest_needs_upgrade?
        user.update_last_login
      end
    end

    #
    # returns a date that is rounded to the quarter.
    #
    def fuzzy_date(date=nil)
      date  ||= Time.now.utc
      year  = date.year
      month = {
        1  => 1,  2  => 1,  3  => 1,
        4  => 4,  5  => 4,  6  => 4,
        7  => 7,  8  => 7,  9  => 7,
        10 => 10, 11 => 10, 12 => 10
      }[date.month]
      Date.new(year, month)
    end

    #
    # update last login date after successful authentication.
    #
    def update_last_login
      new_date = fuzzy_date
      unless self.last_login_on == new_date
        if self.new_record?
          self.last_login_on = new_date
        else
          update_column(:last_login_on, new_date)
        end
      end
    end

    protected

    ##
    ## CALLBACKS
    ##

    # before_create
    def hash_primary_secret
      if new_record? && password
        self.primary_secret = User.new_digest(password)
      end
    end

  end
end
