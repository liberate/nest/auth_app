#
# A ReservedUsername record keeps a username from being reused.
# Typically, this is so that people cannot immediately register
# an email address that was recently used.
#
class ReservedUsername < ActiveRecord::Base
  validates_presence_of :username
  validates_presence_of :domain
  alias_attribute :display_name, :username

  before_validation :set_default_domain

  protected

  def set_default_domain
    self.domain ||= Conf.domain
  end

end
