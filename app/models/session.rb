#
# see: https://github.com/rails/activerecord-session_store
#
# requires the following configuration:
#
#    Rails.application.config.session_store :active_record_store
#    ActionDispatch::Session::ActiveRecordStore.session_class = "Session"
#    ActiveRecord::SessionStore::Session.serializer = :json
#

class Session < ActiveRecord::SessionStore::Session
  belongs_to :user, optional: true

  if defined?(Domain)
    belongs_to :domain, optional: true
  end

  private

  #
  # called before save. ensure our session records are
  # searchable by user and domain.
  #
  def serialize_data!
    self.is_admin  = !!data["admin"]
    self.user_id   ||= data["user_id"]
    if defined?(Domain)
      self.domain_id ||= data["domain_id"]
      if self.user_id && !self.domain_id.present?
        domain_str = User.find(user_id)&.domain
        if domain_str
          self.domain_id = Domain.find_by(domain: domain_str)&.id
        end
      end
    end
    super
  end
end
