
module SessionSweeper
  def self.sweep
    Session.delete_all([
      "is_admin = ? AND (updated_at < ? OR created_at < ?)",
      false,
      words_to_time(Conf.user_session_expiry),
      words_to_time(Conf.user_session_max_lifetime)
    ])
    Session.delete_all([
      "is_admin = ? AND (updated_at < ? OR created_at < ?)",
      true,
      words_to_time(Conf.admin_session_expiry),
      words_to_time(Conf.admin_session_max_lifetime)
    ])
  end

  private

  def self.words_to_time(str)
    Time.now.utc - str.split.inject { |count, unit| count.to_i.send(unit) }
  end
end
