$:.push File.expand_path("lib", __dir__)

require "auth_app/version"

Gem::Specification.new do |spec|
  spec.name        = "auth_app"
  spec.version     = AuthApp::VERSION
  spec.authors     = ["liberate"]
  spec.email       = ["ruby@liberate.org"]
  spec.homepage    = "https://0xacab.org/liberate/nest/auth_app"
  spec.summary     = "A mountable Rails Engine providing basic authentication and session management."
  spec.description = "A mountable Rails Engine providing basic authentication and session management."
  spec.license     = "AGPL"

  spec.files = Dir["{app,config,db,lib}/**/*", "LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", '>= 7'
  spec.add_dependency "haml-rails"
  spec.add_dependency "validates_email_format_of"
  spec.add_dependency "zxcvbn-ruby"
  spec.add_dependency "rbnacl"
  spec.add_dependency "unix-crypt"
  spec.add_dependency "activerecord-session_store"

  # these are actually development dependencies, but we
  # want to be able to run the tests using bin/rails from
  # the main app and development dependencies don't get
  # added to the main app's Gemfile.lock
  spec.add_dependency "sqlite3"
  spec.add_dependency "faker"
  spec.add_dependency "debug"
end
