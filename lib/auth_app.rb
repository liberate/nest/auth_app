require "rbnacl"
require "validates_email_format_of"

require "auth_app/version"
require "auth_app/engine"
require "auth_app/password_tester"
require "auth_app/service"
require "auth_app/service/base"
require "auth_app/service/primary"
require "auth_app/service/recovery_token"
require "auth_app/service/recovery_code"
require "auth_app/service/testing"

module AuthApp
  # home when authenticated
  mattr_accessor :home_url, default: 'home_url'

  # home when not authenticated
  mattr_accessor :root_url, default: 'root_url'

  # if true, email is used for login, and login is not required
  # when resetting a password using an email address
  mattr_accessor :login_is_email, default: false

  # the default domain to use if none is specified.
  mattr_accessor :default_domain, default: nil

  # array of usernames that cannot be used by normal users
  mattr_accessor :forbidden_usernames

  # the from address for outgoing emails
  mattr_accessor :from_address

  # the authentication services that are enabled
  mattr_accessor :services, default: {
    primary: Service::Primary,
    recovery_code: Service::RecoveryCode,
    recovery_token: Service::RecoveryToken,
    testing: ((Rails.env.development? || Rails.env.test?) ? Service::Testing : nil)
  }

  # zxcvbn score:
  # 0 =      too guessable: risky password.
  #                         guesses < 10^3
  #
  # 1 =     very guessable: protection from throttled online attacks.
  #                         guesses < 10^6
  #
  # 2 = somewhat guessable: protection from unthrottled online attacks.
  #                         guesses < 10^8
  #
  # 3 = safely unguessable: moderate protection from offline slow-hash scenario
  #                         guesses < 10^10
  #
  # 4 =   very unguessable: strong protection from offline slow-hash scenario
  #                         guesses >= 10^10
  # Strength of 2 is "somewhat guessable: protection from unthrottled online attacks"
  mattr_accessor :minimum_password_strength_score, default: 2

  # Whether to update user#last_login_on  after sucessfuly log in
  mattr_accessor :update_last_login, default: true

  # This allows users to reset their password using their main email
  mattr_accessor :allow_recovery_from_primary_email, default: true

  # the base controller that all the AuthApp controllers inherit from
  mattr_accessor :base_controller, default: '::ApplicationController'

  # used internally
  mattr_accessor :forbidden_regexps

  class Error < StandardError; end
  class InvalidConfiguration < Error; end
  class UnauthenticatedRequest < Error; end
  class UnauthorizedRequest < Error; end
  class SuspendedRequest < Error; end
  class ForbiddenRequest < Error; end
  class PasswordUpdateError < Error
    attr_reader :errors
    def initialize(errors)
      @errors = errors
      super("Refusing to update an invalid password")
    end
  end

  # argon2 digest from libsodium
  def self.new_digest(pw)
    if pw.nil?
      nil
    else
      RbNaCl::PasswordHash.argon2_str(pw)
    end
  end

  # recovery codes can optionally include '-' and capitals
  def self.parse_random_code(str)
    str.strip.downcase.gsub('-','')
  end

  def self.format_random_code(code, split = 4)
    code.scan(/.{#{split}}/).join('-')
  end

  def self.base_controller
    @@base_controller.constantize
  end
end
