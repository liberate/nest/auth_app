require 'rails/generators'
require 'rails/generators/active_record'

module AuthApp
  #
  # Adds columns if necessary to the users table
  # Run it with +rails g authenticated_app:setup+
  #
  class SetupGenerator < Rails::Generators::Base
    include ActiveRecord::Generators::Migration

    TEMPLATES = File.join(File.dirname(__FILE__), "templates")
    source_paths << TEMPLATES

    desc "Create a migration to add AuthenticatedApp columns to the users table"

    def create_migration_file
      migration_template 'setup_authenticated_app.rb.erb', File.join(db_migrate_path, "setup_authenticated_app.rb")
    end
  end
end
