module AuthApp
  module Service
    # A token emailed to the user, in the form of a clickable link
    # Unlike recovery code which is akin to a secondary password,
    # these are set to expire shortly
    class RecoveryToken < Base
      class_attribute :token_expiration, default: 3
      self.user_password_attribute = :recovery_token
      self.check_confirmation = false
      self.check_strength = false
      self.random_code_length = 8
      self.minimum_length = 8
      self.maximum_length = 8

      def authenticated?
        if user.recovery_token_expires_at.nil? ||
            user.recovery_token_expires_at <= Time.now ||
            !strings_are_equal?(user.recovery_token, password)
          raise UnauthenticatedRequest
        else
          return true
        end
      end

      def update!
        raise "Unsupported"
      end

      def clear!
        super do |user|
          user.write_attribute :recovery_token_expires_at, nil
        end
      end

      def reset!
        user.write_attribute :recovery_token_expires_at, (Time.now + token_expiration.hours)
        user.write_attribute :recovery_token, RandomCode.create(random_code_length)
        user.save!
        return user.recovery_token
      end
    end
  end
end
