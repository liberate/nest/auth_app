#
# This should only be used for testing. Allows setting the primary_secret
# without confirmation and without strength check.
#
module AuthApp
  module Service
    class Testing < Base
      self.user_password_attribute = :primary_secret
      self.check_confirmation = false
      self.check_strength = false
    end
  end
end
