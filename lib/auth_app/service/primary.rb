module AuthApp
  module Service
    # A regular password field, used as the primary account
    # authentication method
    class Primary < Base
      self.user_password_attribute = :primary_secret
      self.check_confirmation = true
      self.check_strength = true

      def digest_needs_upgrade?
        user.read_attribute(user_password_attribute) !~ /\A\$argon2id?\$/
      end
    end
  end
end
