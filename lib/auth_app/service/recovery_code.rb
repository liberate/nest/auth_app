module AuthApp
  module Service
    # A code that can be used to reset the primary password,
    # effectively a second password. Stored in recovery_secret.
    class RecoveryCode < Base
      self.user_password_attribute = :recovery_secret
      self.check_confirmation = false
      self.check_strength = true

      def authenticated?
        super
      rescue UnauthenticatedRequest
        if self.password.present?
          self.password = AuthApp.parse_random_code(self.password)
          super
        else
          raise
        end
      end
    end
  end
end
