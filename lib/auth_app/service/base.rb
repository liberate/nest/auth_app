require "unix_crypt"

module AuthApp
  module Service
    # superclass for password authentication, validation, and modification
    #
    # note that valid? != authenticated?. A password can be invalid
    # but it will still authenticate if the stored digest matches.
    #
    class Base
      class_attribute :minimum_length, default: 8
      # This is set to 60 because the SASL auth daemon can't handle longer passwords
      class_attribute :maximum_length, default: 60
      # Typically the database column for the secret, i.e. primary_secret
      class_attribute :user_password_attribute
      # Validates @password strength with PasswordTester
      class_attribute :check_strength, default: true
      # checks @password_confirmation
      class_attribute :check_confirmation, default: true
      # length of randomly generated password
      class_attribute :random_code_length, default: 20

      include ActiveModel::Validations
      attr_accessor :user, :password, :password_confirmation

      # The user must be valid before password actions
      validates_each :user do |record|
        unless record.user.valid?
          record.errors.merge!(record.user.errors)
        end
      end

      validates_length_of :password, minimum: minimum_length, maximum: maximum_length, allow_blank: false

      # validate password strength
      validates_each :password do |record, attr, val|
        if record.check_strength
          unless PasswordTester.test(val, user_inputs: [record.user.login])
            suggestion = PasswordTester.zxcvbn_suggestion(val, user_inputs: [record.user.login])
            record.errors.add attr, "is not strong enough. #{suggestion}"
          end
        end
      end

      validates_each :password_confirmation do |record, attr, _val|
        if record.check_confirmation && record.password != record.password_confirmation
          record.errors.add attr, "is different from password"
        end
      end

      def initialize(user:, password: nil, password_confirmation: nil)
        self.user = user
        self.password = password
        self.password_confirmation = password_confirmation
      end

      # checks @password against the database hash
      def authenticated?
        digest = user.read_attribute(user_password_attribute)
        raise UnauthenticatedRequest unless self.password.present? && digest.present?
        raise UnauthenticatedRequest unless password_matches_digest?(self.password, digest)
        yield user if block_given?
        true
      end

      alias authenticate! authenticated?

      # updates digest stored in user_password_attribute with @password
      def update!
        if valid?
          User.transaction do
            user.write_attribute user_password_attribute, new_digest(self.password)
            yield user if block_given?
            user.save!
          end
        end
      end

      # removes digest from the database
      def clear!
        User.transaction do
          user.write_attribute user_password_attribute, nil
          yield user if block_given?
          user.save!
        end
        self.password = nil
      end

      # Create a random new password and update the user's digest
      # @return [String] the generated secret
      def reset!
        self.password = RandomCode.create(random_code_length)
        self.password_confirmation = self.password if check_confirmation
        yield user, password if block_given?
        update!
        return password
      end

      def digest_needs_upgrade?
        false
      end

      # This checks authenticated? because it must only be called after we
      # have validated that @password is correct
      def upgrade_digest
        if digest_needs_upgrade? && authenticated?
          user.update_column user_password_attribute, new_digest(self.password)
        end
      end

      protected

      # returns true if the digest was created from the password.
      #
      # Supported password digests:
      #
      # DES: "FinEQbZtgUwjY"
      # MD5: "$1$QoW/tbQq$6JVedc4LtXBZfPjCHGCPl/"
      # SHA256: "$5$7FiVVyf8WlpE7CJb$YCgaoF8oUUxsw.cd9lmKVha8QSVPZeumbC3CUqiHG32"
      # SHA512: "$6$OUMR0gS2w9uJdnG8$yL0z8/y04f9NE7Vsi2tYbLtdhQm1.j2n7xfyHhmU4b..."
      # ARGON2: "$argon2i$v=19$m=32768,t=4,p=1$aRJ9T7ZVYTVIpkJCbCy9bw$hWiOb6QwZ591nV8tXAfqSLuPD8yqI+ZN/B7ajmU33Zc"
      #
      def password_matches_digest?(pw, digest)
        pw = (pw || "").to_s
        if digest.nil?
          false
        elsif digest =~ /\A\$[156]\$/ || digest !~ /\$/
          UnixCrypt.valid?(pw, digest)
        elsif digest =~ /\A\$argon2id?\$/
          RbNaCl::PasswordHash.argon2_valid?(pw, digest)
        else
          false
        end
      end

      #
      # returns true if str1 and str2 are exactly the same.
      # does so in constant time, to prevent timing attacks.
      #
      # str1 and str1 must be less than 32 characters.
      #
      # note: RbNaCl constant time string comparison only
      # works on strings that are 16 or 32 characters.
      # For shorter strings, we right pad the string to make
      # the appropriate length. For longer strings, we
      # raise an exception. Not sure if the padding adds
      # useful timing information.
      #
      def strings_are_equal?(str1, str2)
        unless str1 && str2 && str1.is_a?(String) && str2.is_a?(String)
          return false
        end
        if str1.length <= 16
          str1 = '%-16s' % str1
          str2 = ('%-16s' % str2)[0..15]
          return RbNaCl::Util.verify16(str1, str2)
        elsif str1.length <= 32
          str1 = '%-32s' % str1
          str2 = ('%-32s' % str2)[0..31]
          return RbNaCl::Util.verify32(str1, str2)
        else
          raise ArgumentError, "String length must be less than 32 characters."
        end
      end

      def new_digest(pw)
        AuthApp.new_digest(pw)
      end
    end
  end
end
