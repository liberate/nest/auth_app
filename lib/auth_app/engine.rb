module AuthApp
  class Engine < ::Rails::Engine
    isolate_namespace AuthApp

    #
    # make db:migrate automatically pull in engine migrations:
    #
    initializer :append_migrations do |app|
      unless app.config.root.to_s =~ /test\/dummy/
        app.config.paths["db/migrate"] << config.paths["db/migrate"].first
      end
    end

    #
    # use activerecord session store
    #
    config.after_initialize do
      require 'active_record/session_store'
      Rails.application.config.session_store :active_record_store
      ActionDispatch::Session::ActiveRecordStore.session_class = "Session"
      ActiveRecord::SessionStore::Session.serializer = :json
    end
  end
end
