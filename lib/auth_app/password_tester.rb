require "zxcvbn"

module AuthApp
  module PasswordTester
    PASSWORD_TESTER = Zxcvbn::Tester.new

    def self.test(password, user_inputs: [], minimum_score: AuthApp.minimum_password_strength_score)
      PASSWORD_TESTER.test(password, user_inputs).score >= minimum_score
    end

    def self.zxcvbn_suggestion(password, user_inputs: [])
      PASSWORD_TESTER.test(password, user_inputs).feedback&.suggestions&.first
    end
  end
end
