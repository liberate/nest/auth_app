module AuthApp
  module Service
    #
    # Authenticate with a service-specific password, defaulting to the
    # primary service.
    #
    # Throws UnauthenticatedRequest if invalid otherwise returns true
    #
    # If the password is saved using an old digest, automatically
    # upgrade it.
    #
    # *All* authentication for everything happens through this method.
    #
    # @param user [User] User to authenticate
    # @param password [String] Password to test
    # @param service [Symbol] which service to use. current choices: primary, recovery_code, recovery_token
    # @param fallback [Symbol] another service to try password against
    def self.authenticate!(user:, password:, service: :primary, fallback: nil)
      unless AuthApp.services.key?(service)
        raise InvalidConfiguration, "#{service} is an unsupported service type"
      end

      service = AuthApp.services.fetch(service).new(user: user, password: password)

      if service.authenticated?
        service.upgrade_digest if service.digest_needs_upgrade?
        user.update_last_login if AuthApp.update_last_login
        return true
      end
    rescue UnauthenticatedRequest
      if fallback
        return authenticate!(user: user, password: password, serivce: fallback, fallback: nil)
      else
        raise
      end
    end

    # like authenticate! but doesn't throw when authenticated fails
    def self.authenticated?(**kwargs)
      authenticate!(**kwargs)
    rescue UnauthenticatedRequest
      false
    end
  end
end
