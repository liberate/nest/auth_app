AuthApp::Engine.routes.draw do
  resource :session, only: [:new, :create, :destroy]
  resource :passwords

  # password reset:
  get '/reset-with-code/enter-code', as: :reset_with_code
  put '/reset-with-code/check-code'
  get '/reset-with-code/enter-password'
  put '/reset-with-code/update-password'
  get '/reset-with-email/enter-email', as: :reset_with_email
  put '/reset-with-email/check-email'
  get '/reset-with-email/enter-password'
  put '/reset-with-email/update-password'
  get '/reset-my-password/:recovery_user/:token', to: 'reset_with_email#enter_password', as: 'reset_password_link'
end
