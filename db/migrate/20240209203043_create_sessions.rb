class CreateSessions < ActiveRecord::Migration[7.1]
  def change
    create_table :sessions, if_not_exists: true  do |t|
      t.string :session_id, :null => false
      t.text :data
      t.timestamps
      t.integer :user_id
      t.integer :domain_id
      t.boolean :is_admin, default: false
      t.index :updated_at
      t.index :session_id, :unique => true
    end
  end
end
