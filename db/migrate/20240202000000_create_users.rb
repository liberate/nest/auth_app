class CreateUsers < ActiveRecord::Migration[7.1]
  def change
    create_table "users", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.string   "username",                  limit: 40
      t.string   "display_name",              limit: 191
      t.string   "domain",                    limit: 255
      t.string   "email",                     limit: 191

      t.datetime "created_on"
      t.datetime "updated_on"
      t.date     "last_login_on"

      t.boolean  "is_active",                 default: true
      t.boolean  "is_suspended",              default: false
      t.boolean  "is_pending",                default: false

      t.boolean  "admin"
      t.boolean  "keep",                      default: false
      t.string   "notes",                     limit: 512
      t.integer  "closed_by_id"
      t.string   "closing_note"

      t.string   "primary_secret"
      t.string   "recovery_email"
      t.string   "recovery_token"
      t.datetime "recovery_token_expires_at"
      t.string   "recovery_secret"

      t.index ["email"], name: "index_users_on_email", unique: true
      t.index ["username", "domain"], name: "index_users_on_username_and_domain", unique: true
    end

    create_table "users_access", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci", primary_key: "username" do |t|
      t.datetime "last_attempt"
      t.datetime "last_success"
    end

    create_table "reserved_usernames", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.string  "username"
      t.boolean "api",                     default: false
      t.string  "reserved_by"
      t.boolean "confirmed",               default: false
      t.boolean "deleted",                 default: false
      t.date    "created_on"
      t.string  "domain"
      t.index ["username", "domain"], name: "index_reserved_usernames_on_username_and_domain", unique: true
    end
  end
end