require "faker"

if ENV["COVERAGE"]
  require 'simplecov'
  SimpleCov.start 'rails'
end

# Filter out Minitest backtrace while allowing backtrace from other libraries
# to be shown.
Minitest.backtrace_filter = Minitest::BacktraceFilter.new

module AuthAppUserTestSupport
  def initialize_user
    User.new.tap do |u|
      u.username = Faker::Internet.username
      u.email = Faker::Internet.email
      u.primary_secret = User.new_digest('password')
    end
  end

  def create_user
    initialize_user.tap { _1.save! }
  end
end

class ActiveSupport::TestCase
  include AuthAppUserTestSupport
end

class ActionDispatch::IntegrationTest
  include AuthAppUserTestSupport
  #
  # returns the rest object of a controller, so long as
  # the object follows the normal naming conventions
  #
  # e.g. MembershipsController => @membership
  #
  def instance(name=nil)
    @controller.instance_variable_get(instance_name(name))
  end

  def instance_name(name=nil)
    if name
      "@#{name}"
    else
      "@#{@controller.controller_path.singularize}"
    end
  end
end
