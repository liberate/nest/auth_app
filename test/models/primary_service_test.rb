require_relative '../test_helper.rb'

class PrimaryServiceTest < ActiveSupport::TestCase
  def test_simple_creation
    user = User.create!(username: 'louisemichel', domain: 'new_caledonia.org', password: 'freedom')
    assert user.primary_secret =~ /^\$argon2id\$/
  end

  def test_validates_minimum_length
    user = create_user
    password = 'xon8wro'

    service = AuthApp::Service::Primary.new(user: user,
                                                     password: password,
                                                     password_confirmation: password)
    assert_not service.valid?
    assert service.errors.of_kind? :password, :too_short
  end

  def test_validates_maximum_length
    user = create_user
    password = SecureRandom.hex(32)

    service = AuthApp::Service::Primary.new(user: user,
                                                     password: password,
                                                     password_confirmation: password)
    assert_not service.valid?
    assert service.errors.include?(:password)
    assert service.errors.of_kind? :password, :too_long
  end

  def test_upgrade_digest
    user = initialize_user
    user.primary_secret = UnixCrypt::SHA256.build("password")
    user.save!
    assert AuthApp::Service::Primary.new(user: user, password: "password", password_confirmation: "password").authenticated?
    assert AuthApp::Service::Primary.new(user: user).digest_needs_upgrade?
    AuthApp::Service::Primary.new(user: user, password: "password", password_confirmation: "password").upgrade_digest
    assert /\A\$argon2id?\$/.match?(user.primary_secret)
    assert_not AuthApp::Service::Primary.new(user: user).digest_needs_upgrade?
    assert AuthApp::Service::Primary.new(user: user, password: "password", password_confirmation: "password").authenticated?
  end
end
