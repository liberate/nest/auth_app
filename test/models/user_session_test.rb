require_relative '../test_helper.rb'

class UserSessionTest < ActiveSupport::TestCase
  def setup
    @user = User.create! is_active: true
    @session = Session.create! user: @user, session_id: rand(1000000)
    assert_equal @session, @user.sessions.first
  end

  def test_user_disable
    @user.is_active = false
    @user.save
    assert_nil Session.find_by_id(@session.id), 'disabling user should destroy session'
  end

  def test_user_destroyed
    @user.destroy
    assert_nil Session.find_by_id(@session.id), 'destroying user should destroy session'
  end
end