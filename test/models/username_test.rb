require_relative '../test_helper.rb'

class UserNameTest < ActiveSupport::TestCase
  def test_forbidden_username
    AuthApp.forbidden_usernames = ['root', 'admin*', 'web-master']
    allowed = ['rooty', 'weebmaster']
    not_allowed = ['root', 'r00t', 'administrator', 'webmaster', 'web_master', 'we8.master']
    allowed.each do |name|
      assert User.new(username: name).valid?, "#{name} should be allowed"
    end
    not_allowed.each do |name|
      assert !User.new(username: name).valid?, "#{name} should not be allowed"
    end
  end
end