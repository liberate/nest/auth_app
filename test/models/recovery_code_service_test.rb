require_relative '../test_helper.rb'

class RecoveryCodeServiceTest < ActiveSupport::TestCase
  def test_code_can_be_authenticated
    user = create_user
    code = "TT92-U5HE-9Z6T"
    digest = AuthApp.new_digest(AuthApp.parse_random_code(code))
    user.update_columns recovery_secret: digest

    assert AuthApp::Service::RecoveryCode.new(user: user, password: code).authenticated?
    assert AuthApp::Service::RecoveryCode.new(user: user, password: "tt92u5he9z6t").authenticated?
    assert_raises(AuthApp::UnauthenticatedRequest) do
      AuthApp::Service::RecoveryCode.new(user: user, password: "incorrect").authenticated?
    end
  end

  def test_code_can_be_reset
    user = create_user
    assert_nil user.recovery_secret
    code = AuthApp::Service::RecoveryCode.new(user: user).reset!
    assert AuthApp::Service::RecoveryCode.new(user: user, password: code).authenticated?
  end
end
