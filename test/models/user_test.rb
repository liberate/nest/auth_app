require_relative '../test_helper.rb'

class UserTest < ActiveSupport::TestCase
  def test_validate_recovery_email
    user = initialize_user
    assert user.valid?
    user.recovery_email = "user@"
    assert_not user.valid?
    user.recovery_email = "user@example.com"
    assert user.valid?
  end

  def test_hash_recovery_email_after_save
    user = initialize_user
    user.recovery_email = "user@example.com"
    user.save!
    assert_equal "$argon", user.recovery_email.slice(0, 6)
    assert user.recovery_email_valid?("user@example.com")
  end

  def test_store_two_recovery_emails
    user = initialize_user
    user.recovery_email = "user@example.com,user@example.net"
    user.save!
    assert_equal "$argon", user.recovery_email.slice(0, 6)
    assert user.recovery_email_valid?("user@example.com")
    assert user.recovery_email_valid?("user@example.net")
  end

  def test_checks_legacy_unhashed_recovery_emails
    user = create_user
    user.update_columns :recovery_email => "user@example.com,user@example.net"
    assert_equal "user@example.com,user@example.net", user.recovery_email
    assert user.recovery_email_valid?("user@example.com")
    assert user.recovery_email_valid?("user@example.net")
  end

  def test_primary_email_can_be_used_for_recovery
    user = create_user
    AuthApp.allow_recovery_from_primary_email = false
    assert_not user.recovery_email_valid?(user.email)
    AuthApp.allow_recovery_from_primary_email = true
    assert user.recovery_email_valid?(user.email)
  end

  def test_using_recovery_token_clears_token
    user = create_user
    password = "iRhGIPfC6gPHf9"
    service = AuthApp::Service::RecoveryToken.new(user: user)
    service.reset!
    user.reload
    assert user.authenticated?(user.recovery_token, service: :recovery_token)
    user.reset_password_with_recovery_token!(
      recovery_token: user.recovery_token,
      new_password: password,
      password_confirmation: password
    )
    user.reload
    assert_nil user.recovery_token
    assert_nil user.recovery_token_expires_at
  end
end
