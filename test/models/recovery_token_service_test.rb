require_relative '../test_helper.rb'

class RecoveryTokenServiceTest < ActiveSupport::TestCase
  def test_recovery_token_can_be_authenticated
    user = create_user
    token = RandomCode.create(16)
    user.update_columns recovery_token: token, recovery_token_expires_at: 1.hour.from_now
    assert AuthApp::Service::RecoveryToken.new(user: user, password: token).authenticated?
  end

  def test_recovery_token_is_rejected_when_expired
    user = create_user
    token = RandomCode.create(16)
    user.update_columns recovery_token: AuthApp.new_digest(token), recovery_token_expires_at: 1.minute.ago

    assert_raises(AuthApp::UnauthenticatedRequest) do
      AuthApp::Service::RecoveryToken.new(user: user, password: token).authenticated?
    end
  end

  def test_reset_and_clear_recovery_token
    user = create_user
    assert_nil user.recovery_token
    assert_nil user.recovery_token_expires_at

    token = AuthApp::Service::RecoveryToken.new(user: user).reset!
    assert_equal token, user.recovery_token, "recover token saved must match value returned"
    assert token =~ /[a-z0-9]{8}/, "token should be 8 characters alpha, was #{token}"

    assert AuthApp::Service::RecoveryToken.new(user: user, password: token).authenticated?
    assert user.recovery_token_expires_at.between?(1.minute.ago, (3.1).hours.from_now)

    AuthApp::Service::RecoveryToken.new(user: user).clear!
    assert_nil user.recovery_token
    assert_nil user.recovery_token_expires_at
    assert_raises(AuthApp::UnauthenticatedRequest) do
      AuthApp::Service::RecoveryToken.new(user: user, password: token).authenticated?
    end
  end
end
