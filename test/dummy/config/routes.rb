Rails.application.routes.draw do
  mount AuthApp::Engine => "/auth"
  resources :scoped_things
  resources :multi_scoped_things
  root 'root#show'
end
