class MultiScopedThingsController < ScopedThingsController
  require_authentication allowed_user_scope: ['valid_scope_two', :valid_scope_three]
end
