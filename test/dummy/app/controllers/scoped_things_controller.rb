class ScopedThingsController < ApplicationController
  skip_before_action :check_authentication, only: :create
  require_authentication allowed_user_scope: 'thing'

  def index
    render inline: 'ok'
  end

  def create
    reset_session
    user = User.find(params[:id])
    scoped_log_in(user, params[:scope])
    render inline: 'ok'
  end
end
