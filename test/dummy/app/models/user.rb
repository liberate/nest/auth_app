class User < ActiveRecord::Base
  include AuthApp::UserUsername
  include AuthApp::UserAuthentication
  include AuthApp::UserRecoveryEmail
  include AuthApp::UserSession
end
