# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_02_09_203043) do
  create_table "reserved_usernames", force: :cascade do |t|
    t.string "username"
    t.boolean "api", default: false
    t.string "reserved_by"
    t.boolean "confirmed", default: false
    t.boolean "deleted", default: false
    t.date "created_on"
    t.string "domain"
    t.index ["username", "domain"], name: "index_reserved_usernames_on_username_and_domain", unique: true
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.integer "domain_id"
    t.boolean "is_admin", default: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "username", limit: 40
    t.string "display_name", limit: 191
    t.string "domain", limit: 255
    t.string "email", limit: 191
    t.datetime "created_on"
    t.datetime "updated_on"
    t.date "last_login_on"
    t.boolean "is_active", default: true
    t.boolean "is_suspended", default: false
    t.boolean "is_pending", default: false
    t.boolean "admin"
    t.boolean "keep", default: false
    t.string "notes", limit: 512
    t.integer "closed_by_id"
    t.string "closing_note"
    t.string "primary_secret"
    t.string "recovery_email"
    t.string "recovery_token"
    t.datetime "recovery_token_expires_at"
    t.string "recovery_secret"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["username", "domain"], name: "index_users_on_username_and_domain", unique: true
  end

  create_table "users_access", primary_key: "username", force: :cascade do |t|
    t.datetime "last_attempt"
    t.datetime "last_success"
  end

end
