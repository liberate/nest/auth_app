require_relative '../test_helper.rb'

class SessionsTest < ActionDispatch::IntegrationTest
  def setup
    @user = create_user
  end

  def test_visit_new_session_page
    get "/auth/session/new"
    assert_response :success
  end

  def test_successful_login
    post "/auth/session", params: { login: @user.username, password: 'password' }
    assert_redirected_to "/"
  end

  def test_incorrect_password
    post "/auth/session", params: { login: @user.username, password: 'incorrect' }
    assert_response :success
    assert_equal "Username or password is incorrect.", flash[:danger]
  end

  def test_logout
    post "/auth/session", params: { login: @user.username, password: 'password' }
    assert_response :redirect
    assert_equal session[:user_id], @user.id
    delete "/auth/session"
    assert_redirected_to "/"
    assert_equal "You have been logged out", flash[:info]
    get "/auth/passwords"
    assert_redirected_to "/auth/session/new"
  end
end
