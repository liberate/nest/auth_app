require_relative "../test_helper.rb"

class ResetWithEmailTest < ActionDispatch::IntegrationTest
  def setup
    @user = create_user
    @recovery_email = Faker::Internet.email
    @user.update!(recovery_email: @recovery_email)
  end

  def test_enter_password
    sgid = @user.to_sgid(for: 'recovery_token')

    get '/auth/reset-with-email/enter-password', params: { recovery_user: sgid }
    user = @controller.instance_variable_get('@user')
    assert user, 'should get the user from the sgid'

    # todo, figure out how to ensure this field is empty
    #assert_select 'input[id="token", type="text", value=""]'

    get '/auth/reset-with-email/enter-password'
    user = @controller.instance_variable_get('@user')
    assert_nil user, 'user should be nil when no sgid is specified'
  end

  def test_successfully_reset_password_follow_link
    get "/auth/reset-with-email/enter-email"
    assert_response :success
    assert_select 'form[action="/auth/reset-with-email/check-email"]'
    assert_emails 1 do
      put "/auth/reset-with-email/check-email", params: { recovery_login: @user.login, recovery_email: @recovery_email }
    end
    @user.reload
    last_email  = ActionMailer::Base.deliveries.last
    assert_equal "Reset Password", last_email.subject
    assert_equal @recovery_email, last_email.to.first
    assert_includes last_email.body.to_s, "Someone has requested to a password reset for the account"
    assert_includes last_email.body.to_s, @user.login
    reset_url = last_email.body.to_s[/(\/auth\/reset-my-password\/\S+)\R.*/, 1]
    assert_not_empty reset_url
    get reset_url
    assert_response :success
  end

  def test_successfully_reset_password_enter_code
    get "/auth/reset-with-email/enter-email"
    assert_response :success
    assert_select 'form[action="/auth/reset-with-email/check-email"]'
    assert_emails 1 do
      put "/auth/reset-with-email/check-email", params: { recovery_login: @user.login, recovery_email: @recovery_email }
    end
    @user.reload
    last_email  = ActionMailer::Base.deliveries.last
    assert_equal "Reset Password", last_email.subject
    assert_equal @recovery_email, last_email.to.first
    assert_includes last_email.body.to_s, "Someone has requested to a password reset for the account"
    assert_includes last_email.body.to_s, @user.login
    reset_url = last_email.body.to_s[/(\/auth\/reset-my-password\/\S+)\R.*/, 1]
    assert_not_empty reset_url
    get reset_url
    assert_response :success
  end

  def test_reset_with_email_invalid_recovery_code
  end
end
