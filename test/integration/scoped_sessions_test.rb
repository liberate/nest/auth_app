require_relative '../test_helper.rb'

class ScopedSessionsTest < ActionDispatch::IntegrationTest
  def setup
    @user = create_user
  end

  def login(scope:)
    post scoped_things_url, params: { id: @user.id, scope: scope }
  end

  def multi_login(scope:)
    post multi_scoped_things_url, params: { id: @user.id, scope: scope }
  end

  def test_simple_scoped_user
    login(scope: 'thing')
    get scoped_things_url
    assert_response :success
  end

  def test_scope_mismatch
    login(scope: 'wrong_thing')
    get scoped_things_url
    assert_response :redirect
  end

  def test_multi_scoped_user
    multi_login(scope: 'thing')
    get multi_scoped_things_url
    assert_response :success

    multi_login(scope: 'valid_scope_two')
    get multi_scoped_things_url
    assert_response :success

    multi_login(scope: 'valid_scope_two')
    get scoped_things_url
    assert_response :redirect, 'a valid scope on the subclass controller should not be valid on the parent'

    multi_login(scope: 'valid_scope_three')
    get multi_scoped_things_url
    assert_response :success
  end

  def test_multi_scope_mismatch
    multi_login(scope: 'invalid_scope')
    get multi_scoped_things_url
    assert_response :redirect
  end
end