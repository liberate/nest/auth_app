require_relative "../test_helper.rb"

class ResetWithCodeTest < ActionDispatch::IntegrationTest
  def setup
    @user = create_user
    @code = @user.create_random_code!('password')
  end

  def test_enter_password
    sgid = @user.to_sgid(for: 'recovery_code')

    get '/auth/reset-with-code/enter-password', params: { recovery_user: sgid }
    user = @controller.instance_variable_get('@user')
    assert user, 'should get the user from the sgid'

    get '/auth/reset-with-code/enter-password'
    user = @controller.instance_variable_get('@user')
    assert_nil user, 'user should be nil when no sgid is specified'
  end

  def test_valid_code_and_updating_password
    put "/auth/reset-with-code/check-code", params: { recovery_login: @user.username, recovery_code: @code }
    assert_response :success
    assert_select 'form[action="/auth/reset-with-code/update-password"]'
    assert_not_equal session[:user_id], @user.id
    new_password = Faker::Internet.password
    put "/auth/reset-with-code/update-password", params: {
          recovery_service: "recovery_code",
          recovery_user: @user.to_sgid(for: "recovery_code"),
          recovery_secret: @code,
          new_password: new_password,
          password_confirmation: new_password }
    assert_redirected_to "/"
    assert_equal session[:user_id], @user.id
    assert @user.reload.authenticated?(new_password)
  end

  def test_invalid_code
    get "/auth/reset-with-code/enter-code"
    put "/auth/reset-with-code/check-code", params: { recovery_login: @user.username, recovery_code: RandomCode.create(10) }
    assert_response :success
    assert_select 'form[action="/auth/reset-with-code/check-code"]'
  end

  def test_submitting_weak_password
    new_password = "1234"
    put "/auth/reset-with-code/update-password", params: {
          recovery_service: "recovery_code",
          recovery_user: @user.to_sgid(for: "recovery_code"),
          recovery_secret: @code,
          new_password: new_password,
          password_confirmation: new_password }
    assert_response :success
    assert_select 'form[action="/auth/reset-with-code/update-password"]'
    assert_not @user.reload.authenticated?(new_password)
  end

  def test_invalid_sgid
    password_digest = @user.primary_secret
    new_password = Faker::Internet.password
    put "/auth/reset-with-code/update-password", params: {
          recovery_service: "recovery_code",
          recovery_user: @user.to_sgid(for: "somethingelse"),
          recovery_secret: @code,
          new_password: new_password,
          password_confirmation: new_password }
    assert_nil @controller.instance_variable_get('@user'), 'User should not be set.'
    assert_equal password_digest, @user.reload.primary_secret, 'User password should not change'
    assert_not @user.reload.authenticated?(new_password), 'User should not be able to authenticate'
  end
end
