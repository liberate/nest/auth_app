require_relative "../test_helper.rb"

class PasswordsTest < ActionDispatch::IntegrationTest
  def setup
    @user = create_user
    post "/auth/session", params: { login: @user.username, password: 'password' }
    assert_redirected_to "/"
  end

  def test_show
    get "/auth/passwords"
    assert_response :success
    assert_select 'input[type="password"]', 3
  end

  def test_should_not_update_with_wrong_password
    put "/auth/passwords", params: { update_action: "update_password", current_password: "wrong password" }
    assert_response :success
    assert flash[:danger].present?, flash[:danger]
  end

  def test_should_not_update_without_confirmation
    put "/auth/passwords", params: { update_action: "update_password", current_password: 'password', password: "93g92020h1g" }
    assert_response :success
    assert flash[:danger].present?, flash[:danger]
    @user.reload
    assert @user.authenticated?('password'), "should authenticate with old password"
  end

  def test_should_update_password
    new_pass = "93g92020h1g"
    put "/auth/passwords", params: {
          update_action: "update_password",
          current_password: 'password',
          password: new_pass,
          password_confirmation: new_pass
        }

    assert_response :redirect
    assert_not flash[:danger].present?
    @user.reload
    assert @user.authenticated?(new_pass), "should authenticate with new password"
  end

  def test_rejects_passwords_that_are_too_short
    new_pass = "s1onAjs"
    put "/auth/passwords", params: {
          update_action: "update_password",
          current_password: 'password',
          password: new_pass,
          password_confirmation: new_pass
        }
    assert_response :success
    assert flash[:danger].present?
  end

  def test_should_not_update_recovery_without_confirmation
    assert @user.recovery_secret.blank?
    put "/auth/passwords", params: {update_action: "update_recovery", current_password: "wrong password"}
    assert_response :success
    assert flash[:danger].present?, 'error should be set'
  end

  def test_update_recovery_code
    assert @user.recovery_secret.blank?
    put "/auth/passwords", params: {update_action: "update_recovery", current_password: "password"}
    assert_response :success
    @user.reload
    assert @user.recovery_secret, "recovery_secret should be set"
    assert_match(/argon/, @user.recovery_secret, "recovery code should use argon digest")
  end

  def test_password_not_required_to_clear_old_recovery_email
    @user.update!(recovery_email: "blah@example.org")
    put "/auth/passwords", params: { update_action: "update_recovery_email", clear_value: "true" }
    assert_response :redirect
    assert @user.reload.recovery_email.blank?
  end

  def test_adding_recovery_email_with_wrong_password
    put "/auth/passwords", params: { update_action: "update_recovery_email",
                                     current_password: "wrong password",
                                     user: {recovery_email: 'root@riseup.net'} }
    assert_response :success
    assert flash[:danger].present?, 'should be an error message'
    assert @user.reload.recovery_email.blank?
  end

  def test_submitting_invalid_recovery_email
    assert @user.recovery_email.blank?
    put "/auth/passwords", params: { update_action: "update_recovery_email",
                                     current_password: 'password',
                                     user: {recovery_email: 'root'} }
    assert_response :success
    assert flash[:danger].present?, 'should be an error'
    assert @user.reload.recovery_email.blank?
  end

  def test_update_recovery_email
    assert @user.reload.recovery_email.blank?
    put "/auth/passwords", params: {
          update_action: "update_recovery_email",
          current_password: 'password',
          user: {recovery_email: 'root@riseup.net'}
        }
    assert_response :redirect
    assert @user.reload.recovery_email.present?
  end
end
