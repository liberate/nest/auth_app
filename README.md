# AuthApp

User account management for Ruby on Rails web applications, including authentication and authorizations.

AuthApp is for applications that manage their own user accounts. It is opinionated, and gives you a basic code for user management, authentication and authorization.

AuthApp stores sessions in the database using ActiveRecord session storage. This allows all sessions
to be invalidated if the user logs out, is destroyed, or deactivated.

AuthApp is not for using a third party source of identity to authenticate.

## Features

* Password changing and resetting UI.
* Secure recovery code to allow reset without email.
* Store many types of secrets for user accounts.
* An email provider mode, where user are assumed to not have or need an external email address.

## WARNING

* The password reset mechanism could be used to determine the email addresses of users,
  particularly if the email only option is set. There should be a rate limit or puzzle.

## Installation

Include auth_app and it's required gems in your gemfile:

```ruby
gem "auth_app", git: "https://0xacab.org/liberate/nest/auth_app.git"
```

Run this generator to add the necessary columns to the users table:

``` sh
bundle exec rails g auth_app:setup
```

## API

`AuthApp::Service` represent services that users can use to authenticate with the site.

`AuthApp::Service::Primary` is a regular password-field.

There are currently two recovery services available that can be used to change the primary password

`AuthApp::Service::RecoveryCode` a secondary password that can be provided by or generated for the user

`AuthApp::Service::RecoveryToken` an short-lived token emailed to the user's recovery email address


Example :

``` ruby
user = User.find_by_login("user123")
service = AuthApp::Service::Primary.new(user: user, password: password, password_confirmation: password)

service.authenticated? # is the password correct
service.valid?         # is the password strong enough, adds errors to user
service.update!        # update stored password
service.clear!         # remove password
serivce.upgrade_digest # upgrade digest to argon2 if not already
```

Use `AuthApp::Controller.log_in!(user, password)` to log in a user from a controller. `controller.current_user` provides access to the user model.

## Configuration

`config/initializers/authenticated_app.rb`

``` ruby
AuthApp.home_url = :home_url  # home for authenticated users
AuthApp.root_url = :root_url  # home for unauthenticated users
AuthApp.login_is_email = true # if true, email is used for login, and login is not required
                              # when resetting a password using an email address
AuthApp.default_domain = nil  # the default domain to use if none is specified.
AuthApp.forbidden_usernames = []  # array of strings to disallow as usernames
AuthApp.allow_recovery_from_primary_email = false  # allow recovery using user#email
```

The format for forbidden_usernames is an array of strings, with globbing allowed. For example:

```
AuthApp.forbidden_usernames = [
  'root', 'admin*', 'security', 'web-master'
]
```

With the '*', this will match 'admin' but also 'administrator' and
'administration'. Look alike characters are automatically matched. For example,
'root' also matches 'r00t' and 'admin' also matches 'admln' and 'adm1n'. Finally, any
seperator character such as '-', '.', or '_' will match these separators or no
separator. So, 'web-master' matches 'webmaster', 'web.master', 'web_master', and 'web-master'.


## Integration

In your User class:

``` ruby
class User < ApplicationRecord
  include AuthApp::UserUsername
  include AuthApp::UserAuthentication
  include AuthApp::UserRecoveryEmail
end
```

Include the module `AuthApp::ControllerConcern` in your application controller.

```ruby
class ApplicationController < ActionController::Base
  include AuthApp::ControllerConcern
end
```

To require authentication for a controller, use the class method `require_authentication`.

```ruby
class HomeController < ApplicationController
  require_authentication
end
```

To require authorization for a controller, use the class method
`require_authorization`. An exception will be thrown if the action does not at
some point call `permit! { true }`. This won't rollback any database changes,
so it is mostly useful for catching authorization errors in testing.

```ruby
class AdminHomeController < ApplicationController
  require_authorization

  def show
    permit! { current_user.admin? }
  end

  def index
    # will create an authorization exception because permit! has not been called
  end
end
```

If you want an authorization failure to rollback any database changes, use
`rollback: true`. This uses an `around_action` to wrap the entire action in a
transaction.

```ruby
class AdminHomeController < ApplicationController
  require_authorization rollback: true
  ...
end
```

Finally, mount the engine in your app's `routes.rb`:

```ruby
mount AuthApp::Engine => "/auth"
```

## Development

run tests:

```shell
rake app:db:prepare
rake test
```

view docs: `bundle exec yard server`

